#ifndef BANK_SUBSCRIPTION_H
#define BANK_SUBSCRIPTION_H

#include "constants.h"


struct date
{
    int day;
    int month;
    int year;
};

struct time
{
    int hour;
    int minutes;
    int seconds;

};

struct type
{
    char type;
};

struct cardnumber
{
    int card;
};

struct sum
{
    float sum;
};

#include <string> 
using namespace std;

struct appointment
{
    string app;
};

#endif